# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  result = []
  str.each_char do |ch|
    result << ch if ch == ch.upcase
  end
  result.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    mid = (str.length/2).floor
    return str[mid]
  else
    mid = (str.length/2)-1
    return str[mid] + str[mid+1]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  total =0
  VOWELS.each do |vow|
    total += str.count(vow)
  end
  total
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  (1..num).each { |el| result *= el}
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each do |el|
    result += el + separator
  end
  last_idx = -1 - separator.length
  result[0..last_idx]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = []
str.chars.each.with_index do |el, idx|
  if idx.even?
    result << el.downcase
  else
    result <<el.upcase
  end
end
result.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str = str.split(" ")
  result = []
  str.each do |word|
    if word.length > 4
      result << word.reverse
    else
      result << word
    end
  end
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do |num|
    if num % 15 == 0
      result << "fizzbuzz"
    elsif num % 5 == 0
      result << "buzz"
    elsif num % 3 == 0
      result << "fizz"
    else
      result << num
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  arr.each { |el| result.unshift(el) }
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each do |el|
    if num % el == 0
      return false
    end
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []

  (1..num).each do |el|
    if num % el == 0
      result << el
    end
  end
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors_num = factors(num)
  result = []
  factors_num.each do |el|
    if prime?(el)
      result << el
    end
  end
  result
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  num_evens = 0
  num_odds = 0
  even_num = 0
  odd_num = 0

  arr.each do |num|
    if num.even?
      num_evens +=1
      even_num = num
    else
      num_odds +=1
      odd_num = num_odds
    end
  end
  if num_evens > 1
    return odd_num
  end
  return even_num
end
